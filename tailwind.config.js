/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			aspectRatio: {
				'vertical-video': '9/16'
			}
		}
	},
	plugins: []
};
