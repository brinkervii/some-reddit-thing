import { fileTypeFromFile, type FileTypeResult, type MimeType } from 'file-type';
import fs from 'fs';
import path from 'path';
import { configuration } from '../../../lib/configuration';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = async ({ params }) => {
	const parts = params.path.split('/');
	const fullPath = path.join(configuration.dataDirectory, 'files', ...parts);
	const filename = fullPath.split(path.sep).slice(-1)[0];
	const filenameExt = filename.split('.').slice(-1)[0];

	const defaultFileTypeResult = {
		ext: filenameExt,
		mime: 'application/octet-stream' as MimeType
	} as FileTypeResult;

	const ftr: FileTypeResult = (await fileTypeFromFile(fullPath)) || defaultFileTypeResult;

	return new Response(fs.readFileSync(fullPath), {
		headers: {
			'Content-Type': ftr.mime
		}
	});
};
