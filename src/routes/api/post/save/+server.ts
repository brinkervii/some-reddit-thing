import type { RequestHandler } from '@sveltejs/kit';
import { savePost } from '$lib/jobs/save-post';
import type { RedditPost } from '$lib/model/reddit/subreddit-listing';

export const POST: RequestHandler = async ({ request }) => {
	const redditPost: RedditPost = await request.json();
	const job = await savePost(redditPost);
	const responseBody = JSON.stringify({
		jobId: job.id
	});

	return new Response(responseBody, { status: 200 });
};
