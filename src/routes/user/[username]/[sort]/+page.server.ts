import type { RedditError } from '$lib/model/reddit/reddit-error';
import type { SubredditListing } from '$lib/model/reddit/subreddit-listing';
import { makeUrl } from '$lib/util/url/make-url';
import type { ServerLoad } from '@sveltejs/kit';

export const load: ServerLoad = async ({ params, url }) => {
	const query: { [key: string]: string } = {};

	if (url.searchParams.has('after')) {
		query['after'] = url.searchParams.get('after') as string;
	}

	if (url.searchParams.has('before')) {
		query['before'] = url.searchParams.get('before') as string;
	}

	let baseUrl: string;
	if (params.sort === 'overview') {
		baseUrl = `https://reddit.com/u/${params.username}.json`;
	} else {
		baseUrl = `https://reddit.com/u/${params.username}/${params.sort}.json`;
	}

	const redditUrl = makeUrl(baseUrl, query);
	const redditJson: SubredditListing | RedditError = await (await fetch(redditUrl)).json();

	let redditResult: SubredditListing | null = null;
	let redditError: RedditError | null = null;

	if ((redditJson as RedditError).error) {
		const v = redditJson as RedditError;
		redditError = v;
	} else {
		const v = redditJson as SubredditListing;
		redditResult = v;
	}

	return {
		username: params.username,
		redditResult,
		redditError
	};
};
