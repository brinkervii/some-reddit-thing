import { listJobs } from '$lib/service/job-manager';
import type { ServerLoad } from '@sveltejs/kit';

export const load: ServerLoad = async () => {
	return {
		jobs: await listJobs()
	};
};
