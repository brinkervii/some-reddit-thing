import type { RedditError } from '$lib/model/reddit/reddit-error';
import type { SubredditListing } from '$lib/model/reddit/subreddit-listing';
import { makeUrl } from '$lib/util/url/make-url';

export async function listSubreddit(subreddit: string, sort: string, after?: string) {
	const redditUrl = makeUrl(`https://reddit.com/r/${subreddit}/${sort}.json`, { after });
	const redditJson: SubredditListing | RedditError = await (await fetch(redditUrl)).json();

	let redditResult: SubredditListing | null = null;
	let redditError: RedditError | null = null;

	if ((redditJson as RedditError).error) {
		const v = redditJson as RedditError;
		redditError = v;
	} else {
		const v = redditJson as SubredditListing;
		redditResult = v;
	}

	return {
		subreddit,
		sort,
		redditResult,
		redditError
	};
}
