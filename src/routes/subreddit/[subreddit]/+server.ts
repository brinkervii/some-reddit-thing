import { redirect, type RequestHandler } from '@sveltejs/kit';

export const GET: RequestHandler = ({ params }) => {
	throw redirect(302, `./${params.subreddit}/hot`);
};
