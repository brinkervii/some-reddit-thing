import type { ServerLoad } from '@sveltejs/kit';
import { listSubreddit } from '../../requests';

export const load: ServerLoad = async ({ params }) => {
	return await listSubreddit(params.subreddit as string, params.sort as string);
};
