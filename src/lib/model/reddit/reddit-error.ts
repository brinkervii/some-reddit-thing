export interface RedditError {
	reason: string;
	message: string;
	error: number;
}
