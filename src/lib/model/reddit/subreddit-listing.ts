export interface AuthorFlairRichtextEntry {
	e: string;
	t: string;
}

export interface RedditVideoPreview {
	bitrate_kbps: number;
	fallback_url: string;
	height: number;
	width: 608;
	scrubber_media_url: string;
	dash_url: string;
	duration: number;
	hls_url: string;
	is_gif: boolean;
	transcoding_status: 'completed';
}

export interface SubredditListingPreviewImageEntity {
	url: string;
	width: number;
	height: number;
}

export interface SubredditListingPreviewImage {
	id: string;
	source: SubredditListingPreviewImageEntity;
	resolutions: SubredditListingPreviewImageEntity[];
	variants: {
		obfuscated: {
			source: SubredditListingPreviewImageEntity;
			resolutions: SubredditListingPreviewImageEntity[];
		};
		nsfw: {
			source: SubredditListingPreviewImageEntity;
			resolutions: SubredditListingPreviewImageEntity[];
		};
	};
}

export interface RedditAwardIcon {
	url: string;
	with: number;
	height: number;
}

export interface Awarding {
	giver_coin_reward: null;
	subreddit_id: null;
	is_new: boolean;
	days_of_drip_extension: null;
	coin_price: number;
	id: string;
	penny_donate: null;
	award_sub_type: 'GLOBAL';
	coin_reward: number;
	icon_url: string;
	days_of_premium: null;
	tiers_by_required_awardings: null;
	resized_icons: RedditAwardIcon[];
	icon_width: number;
	static_icon_width: number;
	start_date: null;
	is_enabled: boolean;
	awardings_required_to_grant_benefits: null;
	description: string;
	end_date: null;
	sticky_duration_seconds: null;
	subreddit_coin_reward: number;
	count: number;
	static_icon_height: number;
	name: string;
	resized_static_icons: RedditAwardIcon[];
	icon_format: null;
	icon_height: number;
	penny_price: null;
	award_type: 'global';
	static_icon_url: string;
}

export interface SubredditListingPreview {
	images: SubredditListingPreviewImage[];
	reddit_video_preview?: RedditVideoPreview;
	enabled: boolean;
}

export interface SubredditListingEntryData {
	approved_at_utc: null;
	subreddit: string;
	selftext: string;
	author_fullname: string;
	saved: boolean;
	mod_reason_title?: string;
	gilded: number;
	clicked: boolean;
	title: string;
	link_flair_richtext: [];
	subreddit_name_prefixed: string;
	hidden: boolean;
	pwls: number;
	link_flair_css_class?: string;
	downs: number;
	thumbnail_height?: number;
	top_awarded_type?: number;
	hide_score: boolean;
	name: string;
	quarantine: boolean;
	link_flair_text_color: string;
	upvote_ratio: number;
	author_flair_background_color?: string;
	subreddit_type: 'public' | 'private';
	ups: number;
	total_awards_received: number;
	media_embed: { [key: string]: string };
	thumbnail_width?: number;
	author_flair_template_id: string;
	is_original_content: boolean;
	user_reports?: string[];
	secure_media?: boolean;
	is_reddit_media_domain: boolean;
	is_meta: boolean;
	category?: string;
	secure_media_embed: { [key: string]: string };
	link_flair_text?: string;
	can_mod_post: boolean;
	score: number;
	approved_by?: string;
	is_created_from_ads_ui: boolean;
	author_premium: boolean;
	thumbnail: string | 'self';
	edited: boolean;
	author_flair_css_class: string;
	author_flair_richtext: AuthorFlairRichtextEntry[];
	gildings: { [key: string]: string };
	content_categories?: string[];
	is_self: boolean;
	mod_note?: string;
	created: number;
	link_flair_type: 'text';
	wls: number;
	removed_by_category?: string;
	banned_by: string;
	author_flair_type: 'richtext';
	domain: string;
	allow_live_comments: boolean;
	selftext_html: string;
	likes?: number;
	suggested_sort: null;
	banned_at_utc: null;
	view_count?: number;
	archived: boolean;
	no_follow: boolean;
	is_crosspostable: boolean;
	pinned: boolean;
	over_18: boolean;
	all_awardings: Awarding[];
	awarders: [];
	media_only: boolean;
	can_gild: boolean;
	spoiler: boolean;
	locked: boolean;
	author_flair_text: string;
	treatment_tags: [];
	visited: boolean;
	removed_by: null;
	num_reports: null;
	distinguished: null;
	subreddit_id: string;
	author_is_blocked: boolean;
	mod_reason_by: null;
	removal_reason: null;
	link_flair_background_color: string;
	id: string;
	is_robot_indexable: boolean;
	report_reasons: null;
	author: string;
	discussion_type: null;
	num_comments: number;
	send_replies: boolean;
	whitelist_status: 'all_ads';
	contest_mode: boolean;
	mod_reports: [];
	author_patreon_flair: boolean;
	author_flair_text_color: string;
	permalink: string;
	parent_whitelist_status: 'all_ads';
	stickied: boolean;
	url: string;
	subreddit_subscribers: number;
	created_utc: number;
	num_crossposts: number;
	media: null;
	is_video: boolean;
	preview?: SubredditListingPreview;
	url_overridden_by_dest?: string;
}

export interface SubredditListingEntry {
	kind: 'Listing' | 't3' | 't1';
	data: SubredditListingEntryData;
}

export type RedditPost = SubredditListingEntry;

export interface SubredditListingData {
	after?: string;
	before?: string;
	dist: number;
	modhash: string;
	geo_filter?: string;
	children: SubredditListingEntry[];
}

export interface SubredditListing {
	kind: 'Listing';
	data: SubredditListingData;
}
