import path from 'path';
import PouchDB from 'pouchdb';
import { configuration } from '../configuration';
import type { RedditPost } from '../model/reddit/subreddit-listing';
import { ensureDataDir } from '../util/filesystem/ensure-data-dir';
import type { FileId } from '../util/url/file-id-from-url';

await ensureDataDir();
const db = new PouchDB(path.join(configuration.dataDirectory, 'saved-posts'));

export interface SavedPost {
	_id: string;
	savedAt: Date;
	fileId: FileId;
	post: RedditPost;
	files: string[];
}

export const savedPostRepository = {
	async storeSavedPost(savedPost: SavedPost) {
		await db.put(savedPost);
	},
	async findById(postId: string) {
		return (await db.get(postId)) as SavedPost;
	},
	async postExists(postId: string) {
		try {
			await this.findById(postId);
			return true;
		} catch (e) {
			// TODO: Check for status
			return false;
		}
	}
};
