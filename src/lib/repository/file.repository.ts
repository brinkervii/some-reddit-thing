import blake2 from 'blake2';
import { fileTypeFromFile, type FileTypeResult, type MimeType } from 'file-type';
import fs from 'fs';
import path from 'path';
import PouchDB from 'pouchdb';
import { configuration } from '../configuration';
import { ensureDataDir } from '../util/filesystem/ensure-data-dir';

await ensureDataDir();
const db = new PouchDB(path.join(configuration.dataDirectory, 'file-index'));

export interface IndexedFile {
	_id: string;
	path: string;
	name: string;
	indexedAt: Date;
	hashBlake2s: string;
	mimeType: string;
}

function hashFile(filepath: string): Promise<string> {
	const h = blake2.createHash('blake2s');
	const stream = fs.createReadStream(filepath);

	h.setEncoding('hex');

	return new Promise((resolve, reject) => {
		stream.on('end', () => {
			h.end();
			const digest = h.read();

			resolve(digest);
		});

		stream.on('error', reject);

		stream.pipe(h);
	});
}

export const fileRepository = {
	async index(fileRelativeToDataDir: string) {
		const fullPath = path.join(configuration.dataDirectory, fileRelativeToDataDir);
		const filename = fileRelativeToDataDir.split(path.sep).splice(-1)[0];
		const filenameExt = filename.split('.').slice(-1)[0];

		const defaultFileTypeResult = {
			ext: filenameExt,
			mime: 'application/octet-stream' as MimeType
		} as FileTypeResult;

		const ftr: FileTypeResult = (await fileTypeFromFile(fullPath)) || defaultFileTypeResult;

		const doc: IndexedFile = {
			_id: crypto.randomUUID(),
			path: fileRelativeToDataDir,
			name: filename,
			indexedAt: new Date(),
			hashBlake2s: await hashFile(fullPath),
			mimeType: ftr.mime
		};

		await db.put(doc);

		return doc;
	},
	async getAll() {
		const allDocs = await db.allDocs();

		const response = await db.bulkGet({
			docs: allDocs.rows.map((row) => {
				return { id: row.id };
			})
		});

		return response;
	}
};
