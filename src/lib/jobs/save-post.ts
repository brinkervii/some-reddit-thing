import fs from 'fs';
import path from 'path';
import { configuration } from '../configuration';
import type { RedditPost } from '../model/reddit/subreddit-listing';
import { fileRepository } from '../repository/file.repository';
import { savedPostRepository, type SavedPost } from '../repository/saved-post.repository';
import { scheduleJob } from '../service/job-manager';
import { downloadFile } from '../util/media/download-file';
import { runYtDLP } from '../util/media/yt-dlp';
import { fileIdFromUrl } from '../util/url/file-id-from-url';

async function dataDirectoryForPost(post: RedditPost) {
	const dir = path.join(configuration.dataDirectory, 'files', post.data.id);
	fs.mkdirSync(dir, { recursive: true });

	return dir;
}

async function saveFile(post: RedditPost) {
	await downloadFile(await dataDirectoryForPost(post), post.data.url);
}

async function saveVideo(post: RedditPost) {
	await runYtDLP(await dataDirectoryForPost(post), post.data.url);
}

async function doSavePost(post: RedditPost) {
	const id = post.data.id;
	const fileId = fileIdFromUrl(post.data.url);

	let savedStuff = false;

	if (post.data.url) {
		if (fileId.isVideo) {
			await saveVideo(post);
		} else {
			await saveFile(post);
		}

		savedStuff = true;
	}

	let files: string[] = [];
	if (savedStuff) {
		const dataDirectory = await dataDirectoryForPost(post);
		files = fs.readdirSync(dataDirectory).map((s) => path.join('files', post.data.id, s));

		for (const file of files) {
			await fileRepository.index(file);
		}
	}

	if (await savedPostRepository.postExists(id)) {
		console.log('Post exists', id);
		return;
	}

	const savedPost: SavedPost = {
		_id: id,
		savedAt: new Date(),
		fileId: fileId,
		post: post,
		files: files
	};
	console.log('Saved Post', savedPost._id);

	try {
		await savedPostRepository.storeSavedPost(savedPost);
	} catch (e) {
		let yeet = true;

		if (e && typeof e === 'object') {
			if ('status' in e && e.status == 409) {
				// Post already exists
				yeet = false;
			}
		}

		if (yeet) throw e;
	}

	return savedPost;
}

export async function savePost(post: RedditPost) {
	return await scheduleJob('save-post', doSavePost(post));
}
