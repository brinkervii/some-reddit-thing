import DOMPurify from 'dompurify';
import { marked } from 'marked';

export async function renderMarkdown(input: string): Promise<string> {
	if (!DOMPurify.sanitize) return input;

	try {
		const dirtyHtml = marked(input);
		const cleanHtml = DOMPurify.sanitize(dirtyHtml);

		return cleanHtml;
	} catch (e) {
		console.error(e);
	}

	return input;
}
