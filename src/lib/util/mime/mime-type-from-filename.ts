import { mimeTypeFromExtension } from './mime-type-from-extension';

export function mimeTypeFromFilename(filename: string): string {
	const extension = filename.split('.').slice(-1)[0];
	return mimeTypeFromExtension(extension);
}
