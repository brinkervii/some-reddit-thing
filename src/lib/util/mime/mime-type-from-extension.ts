import db from 'mime-db';

const cache: { [key: string]: string } = {};

export function mimeTypeFromExtension(ext: string): string {
	if (ext in cache) {
		return cache[ext];
	}

	for (const [mimeType, data] of Object.entries(db)) {
		if (data.extensions) {
			if (data.extensions.indexOf(ext) >= 0) {
				cache[ext] = mimeType;

				return mimeType;
			}
		}
	}

	const mimeType = 'application/octet-stream';
	cache[ext] = mimeType;

	return mimeType;
}
