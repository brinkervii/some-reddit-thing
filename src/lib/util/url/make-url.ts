const EMPTY_OBJECT = {};

export function makeUrl(base: string, query?: { [key: string]: string | null | undefined }) {
	const queryList = [];

	for (const [key, value] of Object.entries(query || EMPTY_OBJECT)) {
		if (!value) continue;
		queryList.push(`${key}=${value}`);
	}

	const queryString = queryList.join('&');
	const urlList = [base];

	if (queryString.length > 0) {
		urlList.push(queryString);
	}

	return urlList.join('?');
}
