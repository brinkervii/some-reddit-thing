export type MediaHost =
	| 'unknown'
	| 'imgur'
	| 'redgifs'
	| 'reddit'
	| 'reddit-gallery'
	| 'reddit-images'
	| 'reddit-video'
	| 'youtube'
	| 'youtube-shortlink';

function mediaHostFromUrlPrivate(url?: string): MediaHost {
	if (!url) return 'unknown';

	const parsed = new URL(url);

	switch (parsed.hostname) {
		case 'i.imgur.com':
			return 'imgur';

		case 'www.redgifs.com':
		case 'redgifs.com':
			return 'redgifs';

		case 'i.redd.it':
			return 'reddit-images';

		case 'v.redd.it':
			return 'reddit-video';

		case 'old.reddit.com':
		case 'reddit.com':
		case 'www.reddit.com':
			if (parsed.pathname.split('/').includes('gallery')) return 'reddit-gallery';
			return 'reddit';

		case 'youtube.com':
		case 'm.youtube.com':
			return 'youtube';

		case 'youtu.be':
			return 'youtube-shortlink';

		default:
			return 'unknown';
	}
}

export function mediaHostFromUrl(url?: string): MediaHost {
	const mediaHost = mediaHostFromUrlPrivate(url);

	if (mediaHost === 'unknown') {
		console.warn(`Unknown media host: ${url}`);
	}

	return mediaHost;
}
