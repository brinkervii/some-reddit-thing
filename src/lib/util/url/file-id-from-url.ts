import { mimeTypeFromFilename } from '../mime/mime-type-from-filename';
import { mediaHostFromUrl, type MediaHost } from './media-host-from-url';

const videoHosts: MediaHost[] = ['redgifs', 'reddit-video', 'youtube', 'youtube-shortlink'];

export interface FileId {
	mimeType: string;
	isVideo: boolean;
	isImage: boolean;
	playable: boolean;
	mediaHost: MediaHost;
}

const defaultResponse = {
	mimeType: 'application/octet-stream',
	isImage: false,
	isVideo: false,
	playable: false
} as FileId;

export function fileIdFromUrl(url?: string): FileId {
	if (!url) return defaultResponse;

	const mediaHost = mediaHostFromUrl(url);
	const videoHost = videoHosts.indexOf(mediaHost) >= 0;

	const parsed = new URL(url);
	const filename = parsed.pathname.split('/').slice(-1)[0];
	const mimeType = mimeTypeFromFilename(filename);

	const isGifV = filename.endsWith('.gifv');
	const isGif = filename.endsWith('.gif') || isGifV;
	const isVideo = mimeType.startsWith('video/') || isGifV || videoHost;
	const isImage = (mimeType.startsWith('image/') || isGif) && !isGifV;

	const fid = {
		mimeType,
		isVideo,
		isImage,
		mediaHost,
		playable: isVideo || isGif || videoHost
	};

	return fid;
}
