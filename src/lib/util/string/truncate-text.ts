export function truncateText(
	input: string,
	maxLength = 150,
	ellipse = '...',
	delimiter = ' '
): string {
	if (input.length <= maxLength) {
		return input;
	}

	const words: string[] = [];

	const length = () => {
		let accumulator = words.length;

		for (const w of words) {
			accumulator += w.length;
		}

		return accumulator;
	};

	for (const word of input.split(delimiter)) {
		if (length() >= maxLength) {
			break;
		}

		words.push(word);
	}

	words.push(ellipse);

	return words.join(delimiter);
}
