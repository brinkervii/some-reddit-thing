import fs from 'fs';
import path from 'path';
import { Readable } from 'stream';
import { finished } from 'stream/promises';
import type { ReadableStream } from 'stream/web';

export async function downloadFile(directory: string, url: string) {
	const parsed = new URL(url);
	const filename = parsed.pathname.split('/').slice(-1)[0];
	const filepath = path.join(directory, filename);

	const response = await fetch(url);
	if (!response.body) {
		throw new Error('No response body');
	}

	const body = response.body as ReadableStream;
	const stream = fs.createWriteStream(filepath);

	await finished(Readable.fromWeb(body).pipe(stream));

	return filepath;
}
