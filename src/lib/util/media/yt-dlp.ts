import { spawn } from 'child_process';

export function runYtDLP(directory: string, url: string, video_codec = 'webm'): Promise<void> {
	const thing = spawn('yt-dlp', ['--recode-video', video_codec, url], { cwd: directory });

	thing.stdout.on('data', (data) => {
		console.log(`[yt-dlp][stdout] ${data}`);
	});

	thing.stderr.on('data', (data) => {
		console.error(`[yt-dlp][stdout] ${data}`);
	});

	return new Promise((resolve, reject) => {
		thing.on('error', reject);
		thing.on('close', () => resolve());
	});
}
