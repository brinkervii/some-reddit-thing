import type { SubredditListingPreviewImageEntity } from '$lib/model/reddit/subreddit-listing';

export function fixPreview(
	p: SubredditListingPreviewImageEntity
): SubredditListingPreviewImageEntity {
	const fixed = { ...p };

	fixed.url = fixed.url.replaceAll('&amp;', '&');

	return fixed;
}
