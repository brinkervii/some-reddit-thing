import type {
	SubredditListingPreview,
	SubredditListingPreviewImageEntity
} from '$lib/model/reddit/subreddit-listing';
import { fixPreview } from './fix-preview';

export function pickPreview(
	preview?: SubredditListingPreview
): SubredditListingPreviewImageEntity | null {
	if (!preview) return null;

	const previewsByPixelCount: Map<number, SubredditListingPreviewImageEntity> = new Map();

	if (!(preview.images && preview.images.length > 0)) {
		return null;
	}

	const image = preview.images[0];

	for (const res of image.resolutions || []) {
		previewsByPixelCount.set(res.width * res.height, res);
	}

	if (previewsByPixelCount.size < 1) {
		return fixPreview(image.source);
	} else {
		const biggestPixel = Math.max(...previewsByPixelCount.keys());
		const biggestImage = previewsByPixelCount.get(biggestPixel);

		if (biggestImage) {
			return fixPreview(biggestImage);
		} else {
			return null;
		}
	}
}
