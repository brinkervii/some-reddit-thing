import fs from 'fs';
import { configuration } from '../../configuration';

export async function ensureDataDir() {
	const path = configuration.dataDirectory;

	const exists = fs.existsSync(path);
	let created = false;

	if (!exists) {
		fs.mkdirSync(path, { recursive: true });
		created = true;
	}

	return {
		path,
		exists: fs.existsSync(path),
		madeIt: created
	};
}
