import path from 'path';

export function loadConfiguration() {
	return {
		dataDirectory: path.resolve(process.env.DATA_DIR || 'data')
	};
}

export const configuration = loadConfiguration();
