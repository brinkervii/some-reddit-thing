const jobs: Job<unknown>[] = [];

function generateJobId(): string {
	return crypto.randomUUID();
}

export interface Job<T> {
	id: string;
	kind: string;
	result?: T;
	error?: Error;
	startTime: Date;
	endTime?: Date;
	finished: boolean;
}

export async function scheduleJob<T>(kind: string, work: Promise<T>): Promise<Job<T>> {
	const job: Job<T> = {
		id: generateJobId(),
		kind,
		startTime: new Date(),
		finished: false
	};

	jobs.push(job);

	Promise.resolve(work)
		.then((result) => {
			job.result = result;
			job.finished = true;
			job.endTime = new Date();

			return job;
		})
		.catch((err) => {
			console.error(err);

			job.error = err;
			job.finished = true;
			job.endTime = new Date();
		});

	return job;
}

export async function listJobs() {
	return jobs;
}
